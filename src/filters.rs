use crate::handlers;
use warp::Filter;
use crate::models::MemoneoDeepspeech;

pub fn with_deepspeech_model(
    pool: deadpool::unmanaged::Pool<MemoneoDeepspeech>,
) -> impl Filter<Extract = (deadpool::unmanaged::Pool<MemoneoDeepspeech>,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || pool.clone())
}

pub fn recognize(
    pool: deadpool::unmanaged::Pool<MemoneoDeepspeech>,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("recognize")
        .and(warp::post())
        .and(warp::body::json())
        .and(with_deepspeech_model(pool))
        .and_then(handlers::handle_recognize)
}
