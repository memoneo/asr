use deepspeech::Model;
use serde_derive::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct RecognizeRequestBody {
    #[serde(rename(serialize = "audioContent", deserialize = "audioContent"))]
    pub audio_content: String,
}

#[derive(Deserialize, Serialize)]
pub struct RecognizeResponseBody {
    pub text: String,
}

#[derive(Serialize)]
pub struct OkResponseBody<T> {
    pub message: String,
    pub data: T,
}

impl<T> OkResponseBody<T> {
    pub fn new(message: String, data: T) -> Self {
        OkResponseBody {
            message: message,
            data: data,
        }
    }
}

#[derive(Serialize)]
pub struct ErrorResponseBody {
    pub message: String,
}

impl ErrorResponseBody {
    pub fn new(message: String) -> Self {
        ErrorResponseBody { message: message }
    }
}

pub struct MemoneoDeepspeech {
    pub inner: Model,
}

#[derive(Debug)]
pub struct Base64DecodeError;
impl warp::reject::Reject for Base64DecodeError {}
#[derive(Debug)]
pub struct AudreyReadError;
impl warp::reject::Reject for AudreyReadError {}
#[derive(Debug)]
pub struct TimeoutError;
impl warp::reject::Reject for TimeoutError {}

unsafe impl Send for MemoneoDeepspeech {}

impl MemoneoDeepspeech {
    pub fn new(model: Model) -> Self {
        MemoneoDeepspeech { inner: model }
    }

    pub fn speech_to_text(&mut self, audio_buf: Vec<i16>) -> String {
        self.inner.speech_to_text(&audio_buf).unwrap()
    }
}
