use crate::models::{
    AudreyReadError, Base64DecodeError, ErrorResponseBody, MemoneoDeepspeech, OkResponseBody,
    RecognizeRequestBody, RecognizeResponseBody, TimeoutError,
};
use crate::SAMPLE_RATE;
use audrey::read::Reader;
use audrey::sample::interpolate::{Converter, Linear};
use audrey::sample::signal::{from_iter, Signal};
use std::convert::Infallible;
use warp::http::StatusCode;
use warp::{reject, Rejection};
use std::time::Instant;

pub async fn handle_rejection(err: Rejection) -> Result<impl warp::Reply, Infallible> {
    let code;
    let message;

    if err.is_not_found() {
        code = StatusCode::NOT_FOUND;
        message = "NOT_FOUND";
    } else if let Some(AudreyReadError) = err.find() {
        code = StatusCode::BAD_REQUEST;
        message = "Unable to read audio_content";
    } else if let Some(Base64DecodeError) = err.find() {
        code = StatusCode::BAD_REQUEST;
        message = "Unable to base64 decode audio_content";
    } else if let Some(_) = err.find::<warp::reject::MethodNotAllowed>() {
        code = StatusCode::METHOD_NOT_ALLOWED;
        message = "METHOD_NOT_ALLOWED";
    } else {
        eprintln!("unhandled rejection: {:?}", err);
        code = StatusCode::INTERNAL_SERVER_ERROR;
        message = "UNHANDLED_REJECTION";
    }

    let json = warp::reply::json(&ErrorResponseBody::new(message.into()));

    Ok(warp::reply::with_status(json, code))
}
pub async fn handle_recognize(
    body: RecognizeRequestBody,
    pool: deadpool::unmanaged::Pool<MemoneoDeepspeech>,
) -> Result<impl warp::Reply, Rejection> {
    let start = Instant::now();
    let audio_binary_result = base64::decode(&body.audio_content);
    if let Err(_) = audio_binary_result {
        return Err(reject::custom(Base64DecodeError));
    }
    let audio_binary = audio_binary_result.unwrap();
    let duration = start.elapsed();
    println!("Base64 decode took: {:?}", duration);

    let start = Instant::now();
    let reader_result = Reader::new(std::io::Cursor::new(&audio_binary[..]));
    if let Err(err) = reader_result {
        println!("{}", err);
        return Err(reject::custom(AudreyReadError));
    }
    let mut reader = reader_result.unwrap();
    let desc = reader.description();
    assert_eq!(
        1,
        desc.channel_count(),
        "The channel count is required to be one, at least for now"
    );

    let duration = start.elapsed();
    println!("Audrey read took: {:?}", duration);

    // Obtain the buffer of samples
    let audio_buf: Vec<_> = if desc.sample_rate() == SAMPLE_RATE {
        reader.samples().map(|s| s.unwrap()).collect()
    } else {
        // We need to interpolate to the target sample rate
        let interpolator = Linear::new([0i16], [0]);
        let conv = Converter::from_hz_to_hz(
            from_iter(reader.samples::<i16>().map(|s| [s.unwrap()])),
            interpolator,
            desc.sample_rate() as f64,
            crate::SAMPLE_RATE as f64,
        );
        conv.until_exhausted().map(|v| v[0]).collect()
    };

    let start = Instant::now();
    let model_result = tokio::time::timeout(std::time::Duration::from_secs(5), pool.get()).await;
    if let Err(_) = model_result {
        return Err(reject::custom(TimeoutError));
    }
    let mut model = model_result.unwrap();
    let duration = start.elapsed();
    println!("Model retrieval from pool took: {:?}", duration);


    let start = Instant::now();
    let transcripted_text = model.speech_to_text(audio_buf);

    let duration = start.elapsed();
    println!("Inference took: {:?}", duration);

    Ok(warp::reply::json(&OkResponseBody::new(
        String::from("OK"),
        RecognizeResponseBody {
            text: transcripted_text,
        },
    )))
}
