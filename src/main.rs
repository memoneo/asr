mod filters;
mod handlers;
mod models;

use std::path::Path;

use deepspeech::Model;
use warp::Filter;
use deadpool::unmanaged::Pool;

use models::MemoneoDeepspeech;

const SAMPLE_RATE: u32 = 16_000;

/// gRPC would be conceivable, but is definitely not the bottleneck. Tonic is a nice library for gRPC.
#[tokio::main]
async fn main() {
    let dir_path = Path::new("deepspeech/models");

    let pool = Pool::from(vec![
        MemoneoDeepspeech::new(create_model(&dir_path)),
        MemoneoDeepspeech::new(create_model(&dir_path)),
        MemoneoDeepspeech::new(create_model(&dir_path)),
        MemoneoDeepspeech::new(create_model(&dir_path)),
    ]);

    println!("Warping up server on port 3030");

    let routes = filters::recognize(pool).recover(handlers::handle_rejection);

    warp::serve(routes).run(([127, 0, 0, 1], 3030)).await
}

fn create_model(dir_path: &Path) -> Model {
    Model::load_from_files(&dir_path.join("output_graph.pb")).unwrap()
}
